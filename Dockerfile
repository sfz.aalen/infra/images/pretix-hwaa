FROM docker.io/pretix/standalone:stable
USER root
RUN pip3 install git+https://gitlab.com/sfz.aalen/hackwerk/events/pretix-fontpack-hwaa.git
USER pretixuser
RUN cd /pretix/src && make production